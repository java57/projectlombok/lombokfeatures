## This project contains features provided by Project Lombok.
Believe me, I have used a couple of them and they are very helpful.

I would add these features over here whenever I learn them and update this readme as well.

### val and var
You might have already used them in other languages including JS, val and var behaves in almost same manner.
*  **val:** With val, local variables can be declared, making them final. Data types of val can be inferred from the declaration.
*  **var:** var is similar to val. The only difference is var doesn't create final variables.

### @NonNull
*  **NonNull:** NonNull could be considered as a short form of null check condition.

### @Getter/@Setter
*  **Getter/Setter:** Using these annotations, we don't have to define these methods explicitly. It would be taken care by Lombok automatically.