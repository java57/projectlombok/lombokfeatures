package com.aditya.portfolio.features;

import javafx.util.Pair;
import lombok.val;
import lombok.var;

import java.util.ArrayList;

public class ValAndVar {

    /***
     * Val
     *  local variables can be declared as val.
     *  These variables would be final and type will be inferred from the initialization.
     *
     * Var
     *  Var is similar to val.
     *  The only difference is these variables are not final.
     */
    public static void test(){

        //Creating a final arraylist
        val test = new ArrayList<String>();
        test.add("First");
        test.add("Second");
        System.out.println("Listing data from final arraylist");
        test.forEach(System.out::println);

        // This would give an error as test is a final arraylist.
        //test = new ArrayList<>();

        System.out.println("Listing data using for each loop");
        // Using val to read data from list
        for(val data: test){
            System.out.println(data);
        }

        // Creating another arraylist using var
        var another = new ArrayList<String>();
        another.add("Another first");
        another.add("Another second");
        System.out.println("Listing data from non-final arraylist");
        another.forEach(System.out::println);

        // can create another arraylist object using var
        another = new ArrayList<>();
        another.add("Another third");
        another.add("Another fourth");
        System.out.println("Listing new data");
        another.forEach(System.out::println);
    }
}
