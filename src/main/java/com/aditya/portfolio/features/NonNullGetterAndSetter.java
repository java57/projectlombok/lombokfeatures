package com.aditya.portfolio.features;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class NonNullGetterAndSetter {

    private ChildClass childClass;

    /**
     * NonNull
     *  Setting an object as non null would simply do a null check wherever object is created in this example
     */
    public NonNullGetterAndSetter(@NonNull ChildClass obj){
        this.childClass = obj;
    }

    public static void test(){

        ChildClass childClass = new ChildClass();
        childClass.setAge(1);
        childClass.setName("A");
        NonNullGetterAndSetter obj1 = new NonNullGetterAndSetter(childClass);
        System.out.println("Name: "+obj1.childClass.getName()+" and age: "+obj1.childClass.getAge());

        // This object is going to throw a null pointer exception as we are passing null as ChildClass object
        NonNullGetterAndSetter obj2 = new NonNullGetterAndSetter(null);
        System.out.println("Name: "+obj2.childClass.getName()+" and age: "+obj2.childClass.getAge());
    }

}

/**
 * If we use Getter and Setter, we don't have to explicitly write these methods.
 * It would automatically create these two methods for all parameters in the class.
 */
@Getter
@Setter
class ChildClass{
    private String name;
    private Integer age;
}