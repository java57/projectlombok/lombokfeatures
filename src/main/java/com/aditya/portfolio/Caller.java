package com.aditya.portfolio;

import com.aditya.portfolio.features.NonNullGetterAndSetter;
import com.aditya.portfolio.features.ValAndVar;

public class Caller {

    public static void mainCaller(){
        ValAndVar.test();
        NonNullGetterAndSetter.test();
    }
}
