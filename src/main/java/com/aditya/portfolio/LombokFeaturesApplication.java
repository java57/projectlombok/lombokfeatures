package com.aditya.portfolio;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LombokFeaturesApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(LombokFeaturesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Caller.mainCaller();
	}
}
